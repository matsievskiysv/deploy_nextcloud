arg TAG
from nextcloud:${TAG}

run curl -o /usr/local/share/ca-certificates/letsencrypt_x1.crt https://letsencrypt.org/certs/isrgrootx1.pem \
    && curl -o /usr/local/share/ca-certificates/letsencrypt_x2.crt https://letsencrypt.org/certs/isrg-root-x2.pem \
    && curl -o /usr/local/share/ca-certificates/letsencrypt_e5.crt https://letsencrypt.org/certs/2024/e5.pem \
    && curl -o /usr/local/share/ca-certificates/letsencrypt_e6.crt https://letsencrypt.org/certs/2024/e6.pem \
    && curl -o /usr/local/share/ca-certificates/letsencrypt_r10.crt https://letsencrypt.org/certs/2024/r10.pem \
    && curl -o /usr/local/share/ca-certificates/letsencrypt_r11.crt https://letsencrypt.org/certs/2024/r11.pem \
    && update-ca-certificates
