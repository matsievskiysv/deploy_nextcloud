NextCloud project

# Basic commands

## Bring up

```bash
docker-compose up --no-start
docker-compose start nextcloud_db
sleep 30
docker-compose start
```

## Bring down

```bash
docker-compose down
```

## Enable OpenProject integration

In order to enable OpenProject integration, after the installation issue the command

```bash
docker exec -u 33 -it <container> php occ config:system:set allow_local_remote_servers --value 1
```
